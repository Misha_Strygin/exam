package ru.demon;

/**
 * Created by g16oit18 on 19.12.2017.
 */
public class BankAccount {
    private double balanceAccount;
    private double interestRate;

    BankAccount(double balance, double bet){
        balanceAccount = balance;
        interestRate = bet;
    }
    BankAccount (int balance, double bet){
        balanceAccount = balance;
        interestRate = bet;
    }
    BankAccount(){
        this(0.0, 0.01);
    }

    public String toString(){
        return "Ваш баланс составляет " + balanceAccount + " руб, годовая ставка = " + interestRate + '%';
    }

    void updateBalance(){
        balanceAccount = balanceAccount + (balanceAccount * interestRate/100);
        System.out.println("Новый баланс составляет " + balanceAccount + " руб.");
    }

}
