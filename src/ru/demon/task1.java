package ru.demon;

/**
 * Created by g16oit18 on 19.12.2017.
 */
public class task1 {

    public static void main(String[] args) {
        for (int radius = 1 ; radius <= 5 ; radius++ ){
            double squareCircle = (Math.PI * (radius * radius));
            System.out.println("Площадь круга при r = " + radius + " равна " + squareCircle);
        }

        for (int radius = 1 ; radius <= 4 ; radius++ ) {
            double squareRing = (Math.PI * (radius+1) * (radius+1)) - (Math.PI * (radius * radius));
            System.out.println("Площадь кольца " + radius + " равна " + squareRing);
        }

    }
}
