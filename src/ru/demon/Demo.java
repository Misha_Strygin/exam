package ru.demon;

/**
 * Created by g16oit18 on 19.12.2017.
 */
public class Demo {
    public static void main(String[] args) {
        BankAccount[] bankAccount = new BankAccount[]{
                new BankAccount(100.1 , 1),
                new BankAccount(50 , 15),
                new BankAccount(0.0 , 0.01)};
        for (BankAccount bankAccount1 : bankAccount) {
            System.out.println(bankAccount1);
            bankAccount1.updateBalance();
        }
    }
}
